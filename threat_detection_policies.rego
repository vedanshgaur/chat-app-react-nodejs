package threat_detection

default allow = true

deny {
    input.code_contains_threat_keyword
}
