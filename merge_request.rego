package main

default allow = true

deny {
    input.dependencies["test"]
    input.CI_PIPELINE_SOURCE == "merge_request_event"
    input.CI_MERGE_REQUEST_ASSIGNEES == "vedanshgaur"
    input.CI_COMMIT_AUTHOR == "Vedansh Gaur <vedansh.gaur@bobble.ai>"
}
